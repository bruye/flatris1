FROM node:lts-alpine

RUN mkdir /app
WORKDIR /app
COPY  ./package.json /app
RUN yarn install

WORKDIR /app
COPY . /app


# RUN yarn instasll
#RUN yarn test
RUN yarn build

EXPOSE 3000
CMD yarn start